// This file is part of CaesarIA.
//
// CaesarIA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// CaesarIA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with CaesarIA.  If not, see <http://www.gnu.org/licenses/>.
//
// Copyright 2012-2013 Dalerank, dalerankn8@gmail.com

#include "random_damage.hpp"
#include "game/game.hpp"
#include "city/city.hpp"
#include "game/gamedate.hpp"
#include "objects/house.hpp"
#include "events/dispatcher.hpp"

using namespace constants;

namespace events
{

GameEventPtr RandomDamage::create()
{
  GameEventPtr ret( new RandomDamage() );
  ret->drop();

  return ret;
}

void RandomDamage::_exec( Game& game, unsigned int time )
{
  int population = game.city()->population();
  if( population > _minPopulation && population < _maxPopulation )
  {
    _isDeleted = true;
    HouseList houses;
    houses << game.city()->overlays();

    for( unsigned int k=0; k < houses.size() / 4; k++ )
    {
      HouseList::iterator it = houses.begin();
      std::advance( it, math::random( houses.size() ) );
      (*it)->collapse();
    }
  }
}

bool RandomDamage::_mayExec(Game&, unsigned int) const { return true; }
bool RandomDamage::isDeleted() const {  return _isDeleted; }

void RandomDamage::load(const VariantMap& stream)
{
  VariantList vl = stream.get( "population" ).toList();
  _minPopulation = vl.get( 0, 0 ).toInt();
  _maxPopulation = vl.get( 1, 999999 ).toInt();
}

VariantMap RandomDamage::save() const
{
  VariantMap ret;
  VariantList vl_pop;
  vl_pop << _minPopulation << _maxPopulation;

  ret[ "population" ] = vl_pop;

  return ret;
}

RandomDamage::RandomDamage(){  _isDeleted = false;}

}//end namespace events
