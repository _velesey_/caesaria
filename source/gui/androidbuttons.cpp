// This file is part of CaesarIA.
//
// CaesarIA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// CaesarIA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with CaesarIA.  If not, see <http://www.gnu.org/licenses/>.
//

#include "androidbuttons.hpp"
#include "core/logger.hpp"
#ifdef CAESARIA_PLATFORM_ANDROID
#include <SDL/SDL_screenkeyboard.h>
#endif

namespace gui
{

 AndroidButtons::AndroidButtons(){}
 AndroidButtons::~AndroidButtons(){}
 #ifdef CAESARIA_PLATFORM_ANDROID
	 void AndroidButtons::locate(){
	  if( !SDL_ANDROID_GetScreenKeyboardRedefinedByUser() )
	    {
	      SDL_Rect buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_NUM];

	      for( int i = 0; i < SDL_ANDROID_SCREENKEYBOARD_BUTTON_NUM; i++ )
	       SDL_ANDROID_GetScreenKeyboardButtonPos( i, &buttons[i] );

	  	  int screenW = buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_DPAD2].x + buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_DPAD2].w;
	  	  int screenH = buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_DPAD2].y + buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_DPAD2].h;

	      buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_0].x = screenW / 2 - buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_0].w;
	      buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_0].y = screenH - buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_0].h;

	      buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_1].x = buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_0].x + buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_0].w;
	      buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_1].y = screenH - buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_1].h;

	      buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_2].x = buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_0].x - buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_0].w;
	      buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_2].y =  screenH - buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_2].h;
	       
	      buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_3].x =  0;
	      buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_3].y = screenH / 2 - buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_3].h;

	      buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_4].x =  0;
	      buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_4].y =  buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_3].y - buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_3].h;

	      buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_5].x =  0;
	      buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_5].y = buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_3].y + buttons[SDL_ANDROID_SCREENKEYBOARD_BUTTON_3].h;

	      for( int i = 0; i < SDL_ANDROID_SCREENKEYBOARD_BUTTON_NUM; i++ ){
	       SDL_ANDROID_SetScreenKeyboardButtonPos( i, &buttons[i]  );
	    }
	  }
	}

	 void AndroidButtons::hide(){
	      SDL_ANDROID_SetScreenKeyboardShown(false);
	}

	 void AndroidButtons::show(){
	      SDL_ANDROID_SetScreenKeyboardShown(true);
	}

 #endif
 
}//end namespace gui
